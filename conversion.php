<?php

/* 
 * te-taniguchi
 * 2019/03/01　Ver1
 * 2020/12/18　倉庫が減ったのでカラムも変わった対応
 * 2021/10/24　倉庫が西しかなくなったので対応
 * CSVファイルをJSONに変換
 */

date_default_timezone_set('Asia/Tokyo');

//ファイルの存在確認
$fileName = 'sale_prod_stock.csv';
$csvDir = 'csv_data';
$csvFullPath = $csvDir . DIRECTORY_SEPARATOR  .  $fileName;

$jsonFileName = 'sssv.json';
$jsonDir = 'json_data';
$jsonFullPath = $jsonDir . DIRECTORY_SEPARATOR . $jsonFileName;

if(file_exists($csvFullPath)){
    
} else {
    echo 'nothing';
    exit;
}

//読み込み
$fsObj = new SplFileObject($csvFullPath);
$fsObj->setFlags(SplFileObject::READ_CSV);  //CSVモードで読込

//配列初期化
$myArray = array();

$lineCount = 1;
foreach($fsObj as $line){
    print_r($line);
    //ヘッダスキップ
    if($lineCount === 1){
        $lineCount++;
        continue;
    }
    
    //空データスキップ
    if(empty($line[0])){
        continue;
    }
    
    //カラム6より後は要素から削除
    array_splice($line, 6);
    
    $myArray["item"][] = $line;
    $lineCount++;
}

//ファイル最終更新時間取得
$fileTime = filemtime($csvFullPath);
$myArray['fileTime'] = date('Y/m/d H:i', $fileTime);

//ファイル書き込み
$writeObj = new SplFileObject($jsonFullPath, "w");
$writeObj->fwrite(json_encode($myArray, JSON_UNESCAPED_UNICODE));

echo "success";
//var_dump($myArray);