/* 
 * SUPER SALE STOCK VIEW(SSSV)
 */
/* global fetch */

"use strict;";
$(function () {
    Writer();
});

const ShowZero = true;         //全倉庫の在庫数が0の時は表示フラグ　1→表示　0→しない
const ShowZeroSumary = false;   //最下段の在庫数0を一覧表示フラグ   1→表示　0→しない

function Writer() {

    //fetch       
    fetch("json_data/sssv.json").then(function (res) {
        return res.json();
    }).then(function (myJson) {
        WriteHTML(myJson);
    });
}

function WriteHTML(data) {

//    所々ある、${hoge[0]}という表記は、Template Literalsである。
//    バッククォートで囲んだ所で使用できる。文字列の中に、変数を代入出来る。

    let myItemArray = data['item']; //在庫情報配列
    let fileTimeStamp = data['fileTime'];   //PHPで読み込んだファイル時間

    //数値フォーマットOBJ作成
    const formatter = new Intl.NumberFormat('ja-JP');

    let tag = "";
    let l_name = "";
    let s_name = "";

    for (let line of myItemArray) {

        //大分類
        if (l_name === "") {
            tag += "<details>";
            tag += `<summary>${line[0]}</summary>`;
            l_name = line[0];
        } else if (l_name !== line[0]) {
            if (s_name !== line[2]) {
                tag += "</details>";
            }
            tag += "</details>";
            tag += "<details>";
            tag += `<summary>${line[0]}</summary>`;
            l_name = line[0];
            s_name = "";
        }

        //小分類
        if (s_name === "") {
            tag += "<details>";
            tag += `<summary>${line[2]}</summary>`;
            s_name = line[2];
        } else if (s_name !== line[2]) {
            tag += "</details>";
            tag += "<details>";
            tag += `<summary>${line[2]}</summary>`;
            s_name = line[2];
        }

        //数字フォーマット
        let west = formatter.format(line[5]);
        let east = formatter.format(line[6]);
        let fuku = formatter.format(line[7]);
        let sappo = formatter.format(line[8]);

                
        //在庫ゼロ表示フラグがtrueならゼロでも表示
        if (ShowZero === true) {
            tag += `<div class="row oneline">`;
            tag += `<div class='col-sm-6 itemtext'>${line[3]} ${line[4]}</div>`;
            tag += `<div class='col-sm-6'>${west}</div>`;
            tag += `</div>`;
        } else {    //在庫ゼロフラグが非表示false指定
//            var disp;
//            if (sum === 0) {
//                disp = 'display-none';
//            } else {
//                disp = '';
//            }

            tag += `<div class='row oneline ${disp}' data-sum=${sum}>`;
            tag += `<div class='col-sm-6 itemtext'>${line[3]} ${line[4]}</div>`;
            tag += `<div class='col-sm-6'>${west}</div>`;
            tag += `</div>`;

        }
    }

    tag += "</details></details>";

    $("#maincont").append(tag);
    $("#timeStamp").append(fileTimeStamp);
    if (ShowZeroSumary) {
        WriteZeroItem();
    }
}

//合計在庫数が0のアイテムを、下部に表記する
function WriteZeroItem() {
    //トータルが0の要素objを取得
    let targetArray = $('div[data-sum="0"]');

    $.each(targetArray, function (index, value) {
        $("#info").append($(value).children(".itemtext").text() + "<br>");
    });
}